# afl-ruby Example

AFL for Ruby! You can learn more about AFL itself [here](http://lcamtuf.coredump.cx/afl/).

## Getting Started

### 0. Clone the repo

`afl-ruby` is not yet available on Rubygems, so for now you'll have to clone and build it yourself.

    git clone https://gitlab.com/gitlab-org/security-products/analyzers/fuzzers/afl-ruby.git

### 1. Build the extension

You will need to manually build the native extension to the Ruby interpreter in order to allow AFL to instrument your Ruby code. To do this:

    cd lib/afl
    ruby ../../ext/afl_ext/extconf.rb
    make

### 2. Instrument your code

To instrument your code for AFL, call `AFL.init` when you're ready to initialize the AFL forkserver,
then wrap the block of code that you want to fuzz in `AFL.with_exceptions_as_crashes { ... }`. For
example:

```ruby
def byte
  $stdin.read(1)
end

def c
  r if byte == 'r'
end

def r
  s if byte == 's'
end

def s
  h if byte == 'h'
end

def h
  raise "Crashed"
end

require 'afl'

unless ENV['NO_AFL']
  AFL.init
end

AFL.with_exceptions_as_crashes do
  c if byte == 'c'
  exit!(0)
end
```

### 3. Install AFL

https://github.com/google/AFL

### 4. Run the example

You should then be able to run the sample harness in the `example/` directory.

Because we're using a bog stock ruby interpreter, we must set the environment variable `AFL_SKIP_BIN_CHECK=1` to prevent AFL from checking to see if our binary is instrumented.

    AFL_SKIP_BIN_CHECK=1 /path/to/afl/afl-fuzz -i example/work/input -o example/work/output -- /usr/bin/ruby example/harness.rb

It should only take a few seconds to find a crash. Once a crash is found it should be written to `example/work/output/crashes/` for you to inspect.

### 5. Run the example from CI

The best way to integrate ruby fuzzing with Gitlab CI/CD is by adding additional stage & step to your .gitlab-ci.yml.

```yaml

include:
  - template: Coverage-Fuzzing.gitlab-ci.yml

my_fuzz_target:
  extends: .fuzz_base
  script:
    - apt-get update -qq && apt-get install -y -qq gdb && apt-get install -y -qq git && apt-get install -y -qq ruby-full && apt-get install -y -qq afl++-clang
    - cat /proc/sys/kernel/core_pattern
    - echo core >/proc/sys/kernel/core_pattern
    - git clone https://gitlab.com/atiwari71/afl-ruby.git && cd afl-ruby && gem build afl.gemspec && gem install ./afl-0.0.3.gem && cd -
    - ./gitlab-cov-fuzz run --engine=afl-ruby --regression=false -- example/harness.rb

```

# Credits

This project is a fork from the https://github.com/richo/afl-ruby

Substantial portions of afl-ruby are either inspired by, or transposed directly from afl-python by Jakub Wilk <jwilk@jwilk.net> licensed under MIT.

[Stripe](https://stripe.com) allowed both myself and [rob](https://github.com/robert) to spend substantial amounts of company time developing afl-ruby.

[github-afl]:https://github.com/google/AFL

